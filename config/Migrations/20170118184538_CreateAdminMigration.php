<?php
use Migrations\AbstractMigration;
use Cake\Auth\DefaultPasswordHasher;

class CreateAdminMigration extends AbstractMigration
{
    public $faker;
    public $populator;

    public function up(){

        $this->faker = \Faker\Factory::create();
        $this->populator = new Faker\ORM\CakePHP\Populator($this->faker);

        $this->populator->addEntity('Users', 1, [
            'first_name' => 'José',
            'last_name' => 'Guerrero',
            'email' => 'joseaguerreroh@gmail.com',
            'username' => 'jguerrero',
            'password' => function (){
                return 'secret';
            },
            'birthday' => function (){
                return $this->faker->dateTimeBetween($startDate = '1986-11-24 00:00:00', $endDate = '1986-11-24 23:59:59');
            },
            'role' => 0,
            'active' => 1,
            'created' =>  function (){
                return $this->faker->dateTimeBetween($startDate = 'now', $endDate = 'now');
            }, 
            'modified' =>  function (){
                return $this->faker->dateTimeBetween($startDate = 'now', $endDate = 'now');
            },           
        ]);

        $this->populator->execute();
    }
}
