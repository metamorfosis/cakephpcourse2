<?php
use Migrations\AbstractMigration;
use Cake\Auth\DefaultPasswordHasher;

class CreateUsersSeedMigration extends AbstractMigration
{
    public $faker;
    public $populator;

    public function up(){

        $this->faker = \Faker\Factory::create('es_VE');
        $this->populator = new Faker\ORM\CakePHP\Populator($this->faker);

        $this->populator
            ->addEntity(
                'Users', 100, [
                    'first_name' => function (){
                        return $this->faker->firstName();
                    },
                    'last_name' => function (){
                        return $this->faker->lastName();
                    },
                    'email' => function (){
                        return $this->faker->safeEmail();
                    },                    
                    'username' => function (){
                        return $this->faker->userName();
                    },
                    'password' => function (){
                        return 'secret';
                    },                                        
                    'birthday' => function (){
                        return $this->faker->dateTimeBetween($startDate = '-70 years', $endDate = '-18 years');
                    },
                    'role' => 1,
                    'active' => function (){
                        return rand(0, 1);
                    },                                        
                    'created' => function (){
                        return $this->faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now');
                    },                                        
                    'modified' => function (){
                        return $this->faker->dateTimeBetween($startDate = 'now', $endDate = 'now');
                    }
                ]
            );

            $this->populator->execute();
    }
}
